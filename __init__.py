from trytond.pool import Pool
from .bom import *


def register():
    Pool.register(        
        BOMInput,
        BOMOutput,
        module='bom_product_category', type_='model')