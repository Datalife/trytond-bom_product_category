# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['BOMInput', 'BOMOutput']

__metaclass__ = PoolMeta


class BOMInput:
    __name__ = "production.bom.input"
    product_category = fields.Function(fields.Many2One('product.category', 'Product Category',
                                                       depends=['product']),
                                       'on_change_with_product_category')

    @fields.depends('product')
    def on_change_with_product_category(self, name=None):
        if self.product and self.product.template.category:
            return self.product.template.category.id


class BOMOutput:
    __name__ = "production.bom.output"
    product_category = fields.Function(fields.Many2One('product.category',
                                                       'Product Category',
                                                       depends=['product']),
                                       'on_change_with_product_category')

    @fields.depends('product')
    def on_change_with_product_category(self, name=None):
        if self.product and self.product.template.category:
            return self.product.template.category.id
